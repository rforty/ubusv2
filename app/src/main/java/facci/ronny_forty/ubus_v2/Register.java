package facci.ronny_forty.ubus_v2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import facci.ronny_forty.ubus_v2.ui.login.LoginActivity;

public class Register extends AppCompatActivity {

    Button button_register_2;
    EditText txt_name;
    EditText txt_password;
    EditText txt_email;
    EditText txt_phone;
    EditText txt_facultad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        button_register_2 = findViewById(R.id.button_register_2);
        /*button_register_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(
                        register_activity.this, menu_activity.class);
                startActivity(intent);
            }
        });
        */

        txt_name = (EditText)findViewById(R.id.txt_name_reg);
        txt_email= (EditText)findViewById(R.id.txt_email_register);
        txt_password = (EditText)findViewById(R.id.txt_pswd_register);
        txt_phone = (EditText)findViewById(R.id.txt_phone_reg);
        txt_facultad = (EditText)findViewById(R.id.txt_facultad_reg);
    }
    //Metodo para el boton Registrar
    public void Registrar(View view){
        String nombre = txt_name.getText().toString();
        String email = txt_email.getText().toString();
        String passwd = txt_password.getText().toString();
        String phone = txt_phone.getText().toString();
        String facu= txt_facultad.getText().toString();

        if(nombre.isEmpty() || email.isEmpty() || passwd.isEmpty() || phone.isEmpty() || facu.isEmpty()){
            Toast.makeText(this, "Falta llenar un campo", Toast.LENGTH_LONG).show();
        }else
        if (passwd.length() < 7) {
            Toast.makeText(this, "Debes de ingresar una contraseña de 7 caracteres o mas", Toast.LENGTH_SHORT).show();
        }else
        if (phone.length() != 10) {
            Toast.makeText(this, "Debes de ingresar un numero valido", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Registro en proceso.....", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
        }
    }


}
