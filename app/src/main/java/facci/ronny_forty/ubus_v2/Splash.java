package facci.ronny_forty.ubus_v2;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import facci.ronny_forty.ubus_v2.ui.login.LoginActivity;

public class Splash extends AppCompatActivity {

    static int TIMEOUT_MILLIS = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent (Splash.this, LoginActivity.class);
                startActivity(i);
                //finish();
            }
        }, TIMEOUT_MILLIS);
    }
}
